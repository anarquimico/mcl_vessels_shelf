MineClone2 mod: vessels_shelf
==========================
See license.txt for license information.
---------------------------
Author of media (texture): Vanessa Ezekowitz (CC BY-SA 3.0)
---------------------------
This mod implements Vessels Shelf for brewing items in MineClone2, it's an adaptation from the Vessels Shelf present in Minetest Game.
The code is basically from mcl_bookshelf along with some mcl_utils functions.
