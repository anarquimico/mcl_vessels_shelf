
local S = minetest.get_translator("vessels")
local C = minetest.colorize
local F = minetest.formspec_escape


local function close_forms(pos)
	local players = minetest.get_connected_players()
	local formname = "mcl_vessels_shelf:vessels_shelf_"..pos.x.."_"..pos.y.."_"..pos.z
	for p = 1, #players do
		if vector.distance(players[p]:get_pos(), pos) <= 30 then
			minetest.close_formspec(players[p]:get_player_name(), formname)
		end
	end
end


local function drop_item_stack(pos, stack)
	if not stack or stack:is_empty() then return end
	local drop_offset = vector.new(math.random() - 0.5, 0, math.random() - 0.5)
	minetest.add_item(vector.add(pos, drop_offset), stack)
end


function drop_items_from_meta_container(listname)
	return function(pos, oldnode, oldmetadata)
		if oldmetadata and oldmetadata.inventory then
			-- process in after_dig_node callback
			local main = oldmetadata.inventory.main
			if not main then return end
			for _, stack in pairs(main) do
				drop_item_stack(pos, stack)
			end
		else
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			for i = 1, inv:get_size( "main") do
				drop_item_stack(pos, inv:get_stack( "main", i))
			end
			meta:from_table()
		end
	end
end


local drop_content = drop_items_from_meta_container( "main")

local function on_blast(pos)
	local node = minetest.get_node(pos)
	drop_content(pos, node)
	minetest.remove_node(pos)
end

local function protection_check_move(pos, from_list, from_index, to_list, to_index, count, player)
	local name = player:get_player_name()
	if minetest.is_protected(pos, name) then
		minetest.record_protection_violation(pos, name)
		return 0
	else
		return count
	end
end

local vessels = { "mcl_potions:glass_bottle", "mcl_potions:dragon_breath", "mcl_honey:honey_bottle", "mcl_experience:bottle",
			      "mcl_potions:water", "mcl_potions:water_lingering", "mcl_potions:water_splash",
				  "mcl_potions:river_water",
				  "mcl_potions:awkward", "mcl_potions:awkward_lingering", "mcl_potions:awkward_splash",
				  "mcl_potions:mundane", "mcl_potions:mundane_lingering", "mcl_potions:mundane_splash",
				  "mcl_potions:thick", "mcl_potions:thick_lingering", "mcl_potions:thick_splash",
				  "mcl_potions:slowness", "mcl_potions:slowness_plus", "mcl_potions:slowness_splash", "mcl_potions:slowness_lingering", "mcl_potions:slowness_plus_lingering", "mcl_potions:slowness_plus_splash", "mcl_potions:slowness_2", "mcl_potions:slowness_2_splash", "mcl_potions:slowness_2_lingering",
				  "mcl_potions:healing", "mcl_potions:healing_plus", "mcl_potions:healing_splash", "mcl_potions:healing_lingering", "mcl_potions:healing_plus_lingering", "mcl_potions:healing_plus_splash", "mcl_potions:healing_2", "mcl_potions:healing_2_splash", "mcl_potions:healing_2_lingering",
				  "mcl_potions:night_vision", "mcl_potions:night_vision_plus", "mcl_potions:night_vision_splash", "mcl_potions:night_vision_lingering", "mcl_potions:night_vision_plus_lingering", "mcl_potions:night_vision_plus_splash", "mcl_potions:night_vision_2", "mcl_potions:night_vision_2_splash", "mcl_potions:night_vision_2_lingering",
				  "mcl_potions:swiftness", "mcl_potions:swiftness_plus", "mcl_potions:swiftness_splash", "mcl_potions:swiftness_lingering", "mcl_potions:swiftness_plus_lingering", "mcl_potions:swiftness_plus_splash", "mcl_potions:swiftness_2", "mcl_potions:swiftness_2_splash", "mcl_potions:swiftness_2_lingering",
				  "mcl_potions:fire_resistance", "mcl_potions:fire_resistance_plus", "mcl_potions:fire_resistance_splash", "mcl_potions:fire_resistance_lingering", "mcl_potions:fire_resistance_plus_lingering", "mcl_potions:fire_resistance_plus_splash", "mcl_potions:fire_resistance_2", "mcl_potions:fire_resistance_2_splash", "mcl_potions:fire_resistance_2_lingering",
				  "mcl_potions:water_breathing", "mcl_potions:water_breathing_plus", "mcl_potions:water_breathing_splash", "mcl_potions:water_breathing_lingering", "mcl_potions:water_breathing_plus_lingering", "mcl_potions:water_breathing_plus_splash", "mcl_potions:water_breathing_2", "mcl_potions:water_breathing_2_splash", "mcl_potions:water_breathing_2_lingering",
				  "mcl_potions:regeneration", "mcl_potions:regeneration_plus", "mcl_potions:regeneration_splash", "mcl_potions:regeneration_lingering", "mcl_potions:regeneration_plus_lingering", "mcl_potions:regeneration_plus_splash", "mcl_potions:regeneration_2", "mcl_potions:regeneration_2_splash", "mcl_potions:regeneration_2_lingering",
				  "mcl_potions:poison", "mcl_potions:poison_plus", "mcl_potions:poison_splash", "mcl_potions:poison_lingering", "mcl_potions:poison_plus_lingering", "mcl_potions:poison_plus_splash", "mcl_potions:poison_2", "mcl_potions:poison_2_splash", "mcl_potions:poison_2_lingering",
				  "mcl_potions:leaping", "mcl_potions:leaping_plus", "mcl_potions:leaping_splash", "mcl_potions:leaping_lingering", "mcl_potions:leaping_plus_lingering", "mcl_potions:leaping_plus_splash", "mcl_potions:leaping_2", "mcl_potions:leaping_2_splash", "mcl_potions:leaping_2_lingering",
				  "mcl_potions:harming", "mcl_potions:harming_plus", "mcl_potions:harming_splash", "mcl_potions:harming_lingering", "mcl_potions:harming_plus_lingering", "mcl_potions:harming_plus_splash", "mcl_potions:harming_2", "mcl_potions:harming_2_splash", "mcl_potions:harming_2_lingering",
				  "mcl_potions:invisibility", "mcl_potions:invisibility_plus", "mcl_potions:invisibility_splash", "mcl_potions:invisibility_lingering", "mcl_potions:invisibility_plus_lingering", "mcl_potions:invisibility_plus_splash", "mcl_potions:invisibility_2", "mcl_potions:invisibility_2_splash", "mcl_potions:invisibility_2_lingering",
			  }

local function is_vessel (tab, val)
	for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end

    return false
end

local function protection_check_put_take(pos, listname, index, stack, player)
	local name = player:get_player_name()
	if minetest.is_protected(pos, name) then
		minetest.record_protection_violation(pos, name)
		return 0
	-- elseif minetest.get_item_group(stack:get_name(), "brewitem") ~= 0 or stack:get_name() == "mcl_honey:honey_bottle" or stack:get_name() == "mcl_experience:bottle" then
	elseif is_vessel(vessels, stack:get_name()) then
		return stack:get_count()
	else
		return 0
	end
end


local function vessels_shelf_gui(pos, node, clicker)
	local name = minetest.get_meta(pos):get_string("name")

	if name == "" then
		name = S( "Vessels Shelf")
	end

	local playername = clicker:get_player_name()

	minetest.show_formspec(playername,
		"mcl_vessels_shelf:vessels_shelf_"..pos.x.."_"..pos.y.."_"..pos.z,
		table.concat({
			"size[9,8.75]",
			"label[0,0;"..F(C("#313131", name)).."]",
			"list[nodemeta:"..pos.x..","..pos.y..","..pos.z..";main;0,0.5;9,3;]",
			mcl_formspec.get_itemslot_bg(0, 0.5, 9, 3),
			"label[0,4.0;"..F(C( "#313131", S( "Inventory"))).."]",
			"list[current_player;main;0,4.5;9,3;9]",
			mcl_formspec.get_itemslot_bg(0, 4.5, 9, 3),
			"list[current_player;main;0,7.74;9,1;]",
			mcl_formspec.get_itemslot_bg(0, 7.74, 9, 1),
			"listring[nodemeta:"..pos.x..","..pos.y..","..pos.z..";main]",
			"listring[current_player;main]",
		})
	)
end

local function close_forms(pos)
	local players = minetest.get_connected_players()
	local formname = "mcl_vessels_shelf:vessels_shelf_"..pos.x.."_"..pos.y.."_"..pos.z
	for p = 1, #players do
		if vector.distance(players[p]:get_pos(), pos) <= 30 then
			minetest.close_formspec(players[p]:get_player_name(), formname)
		end
	end
end



minetest.register_node( "mcl_vessels_shelf:vessels_shelf", {
	description = S( "Vessels Shelf" ),
	tiles = {"default_wood.png", "default_wood.png", "default_wood.png",
		"default_wood.png", "default_wood.png", "vessels_shelf.png"},
	stack_max = 64,
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {
		handy=1, axey=1, deco_block=1, material_wood=1,
		flammable=3, fire_encouragement=30, fire_flammability=20, container=1},
	drop = "mcl_vessels_shelf:vessels_shelf",
	sound = wood_sound,
	_mcl_blast_resistance = 1.5,
	_mcl_hardness = 1.5,
	_mcl_silk_touch_drop = true,
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		inv:set_size( "main", 9*3)
	end,
	after_place_node = function(pos, placer, itemstack, pointed_thing)
		minetest.get_meta(pos):set_string( "name", itemstack:get_meta():get_string( "name"))
	end,
	allow_metadata_inventory_move = protection_check_move,
	allow_metadata_inventory_take = protection_check_put_take,
	allow_metadata_inventory_put = protection_check_put_take,
	on_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
		minetest.log( "action", player:get_player_name()..
			" moves stuff in vessels shelf at "..minetest.pos_to_string(pos))
	end,
	on_metadata_inventory_put = function(pos, listname, index, stack, player)
		minetest.log( "action", player:get_player_name()..
			" moves stuff to vessels shelf at "..minetest.pos_to_string(pos))
	end,
	on_metadata_inventory_take = function(pos, listname, index, stack, player)
		minetest.log( "action", player:get_player_name()..
			" takes stuff from vessels shelf at "..minetest.pos_to_string(pos))
	end,
	after_dig_node = drop_content,
	on_blast = on_blast,
	on_rightclick = vessels_shelf_gui,
	on_destruct = close_forms,
})

minetest.register_craft({
	output = "mcl_vessels_shelf:vessels_shelf",
	recipe = {
		{ "group:wood", "group:wood", "group:wood" },
		{ "mcl_potions:glass_bottle", "mcl_potions:glass_bottle", "mcl_potions:glass_bottle" },
		{ "group:wood", "group:wood", "group:wood" },
	}
})

minetest.register_craft({
	type = "fuel",
	recipe = "mcl_vessels_shelf:vessels_shelf",
	burntime = 15,
})
